# -*- coding: utf-8 -*-

##############################################################################
#
#    Albin John.
#    Copyright (C) 2020-TODAY Albin John(<https://www.linkedin.com/in/albin-john/>).
#    Author: Albin John(<albinjohn54@gmail.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <https://www.gnu.org/licenses/>.
#
##############################################################################

{
	'name': 'Customer Approval',
	'category': 'Sale',
	'author': 'Albin John',
	'website':'https://www.linkedin.com/in/albin-john/',
	'version': '0.1',
	'license': 'LGPL-3',
	'depends': [
		'sale',
		'sale_management',
	],
	'summary': """
	eJam Test Task - Senior Odoo Developer <Albin>
	""",
	'description': """
		The task is detailed as follows:
		1. Install the community version of Odoo 13.0
		2. Create a new customer (through create and edit) while creating a sale order
		3. This customer should only be visible under the Draft Customer menu
		4. This record should move from the Draft Customer menu to the Customer menu after it
		is approved by an Administrator 
	""",
	'data': [
		'views/res_partner_view.xml',
	],
	'qweb': [
		
	],
	'installable': True,
	'auto_install': False,
	'application': False,
}