# -*- coding: utf-8 -*-
# © 2020 - TODAY  Albin John

import logging

from odoo import api, fields, models, _

_logger = logging.getLogger(__name__)

class ResPartner(models.Model):
	_inherit = 'res.partner'

	status = fields.Selection(string='Status', selection=[('draft', 'Draft'), ('approved', 'Approved')], default='draft')

	def action_approve_customer(self):
		return self.write({'status': 'approved'})