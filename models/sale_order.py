# -*- coding: utf-8 -*-
# © 2020 - TODAY  Albin John

import logging

from odoo import api, fields, models, _
from odoo.exceptions import  UserError

_logger = logging.getLogger(__name__)

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	def action_confirm(self):
		if self.partner_id.status != 'approved':
			raise UserError(_('It is not allowed to confirm an order for a customer with draft status'))
		return super(SaleOrder, self).action_confirm()