# eJam Test Task - Senior Odoo Developer <Albin>
The task is detailed as follows:
1. Install the community version of Odoo 13.0
2. Create a new customer (through create and edit) while creating a sale order
3. This customer should only be visible under the Draft Customer menu
4. This record should move from the Draft Customer menu to the Customer menu after it is approved by an Administrator